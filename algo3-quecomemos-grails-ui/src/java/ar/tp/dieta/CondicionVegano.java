package ar.tp.dieta;

import ar.tp.dieta.Condicion;
import ar.tp.dieta.ElementoDeReceta;
import ar.tp.dieta.Usuario;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("all")
public class CondicionVegano extends Condicion {
  public List<String> elementosNoRecomendables = new ArrayList<String>();
  
  public CondicionVegano() {
    this.elementosNoRecomendables.add("pollo");
    this.elementosNoRecomendables.add("carne");
    this.elementosNoRecomendables.add("chivito");
    this.elementosNoRecomendables.add("chori");
    this.elementosNoRecomendables.add("lomo");
  }
  
  @Override
  public boolean esVegano() {
    return true;
  }
  
  @Override
  public boolean seSubsana(final Usuario unUsuario) {
    return unUsuario.meGustaLaFruta();
  }
  
  @Override
  public boolean validarCondicion(final Usuario unUsuario) {
    boolean _meGustaLaCarne = unUsuario.meGustaLaCarne();
    return (!_meGustaLaCarne);
  }
  
  @Override
  public boolean ingredienteEsInadecuado(final ElementoDeReceta unElemento) {
    String _nombre = unElemento.getNombre();
    return this.elementosNoRecomendables.contains(_nombre);
  }
}
